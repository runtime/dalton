;; publish.el
;; Emacs publish file for project.
;; Run the following command to execute:
;; emacs --batch --no-init-file --load publish.el --funcall org-publish-all

;; Packages:
(require 'package)
(require 'ox-publish)
(require 'ox-extra)
(require 'org)
(require 'htmlize)

(setq org-src-fontify-natively t)
;; Source code block configuration:
(org-babel-do-load-languages
 'org-babel-load-languages
 '(
   (C . t)
   (fortran . t)
   (python . t)
   (shell . t)
  ))

(setq org-publish-project-alist
      `(("website"
         :base-directory "."
         :base-extension "org"
         :publishing-directory "."
         :publishing-function org-html-publish-to-html
         )))

;; -*- emacs-lisp -*-
(org-add-link-type
 "color"
 (lambda (path)
   (message (concat "color "
                    (progn (add-text-properties
                            0 (length path)
                            (list 'face `((t (:foreground ,path))))
                            path) path))))
 (lambda (path desc format)
   (cond
    ((eq format 'html)
     (format "<span style=\"color:%s;\">%s</span>" path desc))
    ((eq format 'latex)
     (format "{\\color{%s}%s}" path desc)))))

