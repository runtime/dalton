* Node reservation
:PROPERTIES:
:CUSTOM_ID: reserve
:END:

There is no reservation system per se (we don't prevent people from logging it) but a system to inform other users. By default, when one connects, the list of connected people shows up:
#+begin_src sh :eval never-export
### Warning: host is used by: wacren ###
#+end_src

If somebody reserved the machine, the message turns into:
#+begin_src sh :eval never-export
#######################################
### This node is currently reserved ###
#### user: rnamyst
#### begin: 2010-07-20 10:01:25+02:00
#### end: 2010-07-20 10:16:25+02:00
### Please do not use this machine  ###
#######################################
#+end_src

To reserve a node:
#+begin_src sh :eval never-export
$ reserve-node.sh
Reserving until 2010-07-20 10:16:25+02:00
#+end_src

This displays a big warning on all opened terminals and in all new ssh sessions that would be opened during the reservation.

By default the reservation stops 15m later. One can specify another duration as an argument:
#+begin_src sh :eval never-export
$ reserve-node.sh 2h
$ reserve-node.sh 35m
#+end_src

The reservation duration is limited to 2h during the day, 8h in the evening (after 20:00), 4h early in the morning (before 4:00) and 24h during the week-end. However, a reservation can be renewed by restarting the reservation script to update the expiration date. Lastly, one can cancel with:
#+begin_src sh :eval never-export
$ unreserve-node.sh
#+end_src

To make sure not to be disturbed, and notably for long reservations or deadline periods, send a small mail to [mailto:dalton-users@inria.fr](dalton-users).

If a buildbot is currently running, one can disable it by logging in as benchmarks user and starting buildslave stop from ~benchmarks/buildbot/slaves/bidule
